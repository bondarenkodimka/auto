package com.example.auto;

import com.example.auto.domain.CarEntity;
import com.example.auto.domain.CarRequest;
import com.example.auto.domain.CarResponse;
import org.springframework.stereotype.Component;

@Component
public class Converter {
    public CarEntity toCarEntity(CarRequest car){
        return new CarEntity(car.getBrand(), car.getModel(), car.getPrice());
    }

    public CarResponse toCarResponse(CarEntity car){
        return new CarResponse(car.getId(), car.getBrand(), car.getModel(), car.getPrice());
    }
}
