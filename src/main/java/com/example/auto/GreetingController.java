package com.example.auto;

import com.example.auto.domain.CarEntity;
import com.example.auto.domain.CarRequest;
import com.example.auto.domain.CarResponse;
import com.example.auto.repos.CarRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cars")
public class GreetingController {
    @Autowired
    private CarRepo carRepo;

    @Autowired
    private Converter converter;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<CarEntity> add(@RequestBody CarRequest car) {
        CarEntity carEntity = converter.toCarEntity(car);
        carRepo.save(carEntity);
        return new ResponseEntity<CarEntity>(carEntity, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<CarEntity> changePrice(@RequestBody CarEntity car) {
        if (carRepo.existsById(car.getId())){
            carRepo.save(car);
            return new ResponseEntity<CarEntity>(car, HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<CarEntity>(car, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CarResponse get(@PathVariable String id) {
        CarEntity carEntity = carRepo.findById(id).get();
        return converter.toCarResponse(carEntity);
    }
}
