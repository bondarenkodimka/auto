package com.example.auto.repos;

import com.example.auto.domain.CarEntity;
import org.springframework.data.repository.CrudRepository;

public interface CarRepo extends CrudRepository<CarEntity, String> {

}
